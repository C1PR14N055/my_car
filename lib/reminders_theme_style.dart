// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'package:flutter/material.dart';

class RemindersTheme {
  static get theme {
    final originalTextTheme = ThemeData.light().textTheme;
    final originalBody1 = originalTextTheme.body1;

    return ThemeData.light().copyWith(
      primaryColor: Colors.red,
      accentColor: Colors.redAccent,
      buttonColor: Colors.redAccent,
      textSelectionColor: Colors.redAccent,
      backgroundColor: Colors.white,
      toggleableActiveColor: Colors.redAccent,
      textTheme: originalTextTheme.copyWith(
        title: originalBody1.copyWith(decorationColor: Colors.transparent, color: Color(0xFF757575)),
        subhead: originalBody1.copyWith(decorationColor: Colors.transparent, color: Color(0xFF757575)),
      ),
    );
  }
}

class RemindersStyle {
  static final baseTextStyle = TextStyle(fontFamily: 'Poppins');
  static final smallTextStyle = commonTextStyle.copyWith(
    fontSize: 12.0,
  );
  static final commonTextStyle = baseTextStyle.copyWith(
    color: Colors.white,
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
  );

  static final titleTextStyle = baseTextStyle.copyWith(
    color: Colors.white,
    fontSize: 18.0,
    fontWeight: FontWeight.w600,
  );

  static final headerTextStyle = baseTextStyle.copyWith(
    color: Color(0xFF757575),
    fontSize: 20.0,
    fontWeight: FontWeight.w400,
  );
}