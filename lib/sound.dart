import 'dart:async';
import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/services.dart' show rootBundle;

class Sound {
  static const String _soundsPath = 'assets/sounds';
  static AudioPlayer _audioPlayer = new AudioPlayer();

  static Future playLocal(localFileName) async {
    final dir = await getApplicationDocumentsDirectory();
    final file = new File('${dir.path}/$localFileName');
    if (!(await file.exists())) {
      final soundData = await rootBundle.load('$_soundsPath/$localFileName');
      final bytes = soundData.buffer.asUint8List();
      await file.writeAsBytes(bytes, flush: true);
    }
    await _audioPlayer.stop();
    await _audioPlayer.play(file.path, isLocal: true);
  }
}
