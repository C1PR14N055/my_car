// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'package:my_car/file_storage.dart';

class PrefsEntity {
  bool shownAddReminderFeature;
  bool shownShareAppFeature;
  int nrRemindersAdded;
  int nrTimesShared;

  PrefsEntity({
    this.nrRemindersAdded = 0,
    this.nrTimesShared = 0,
    this.shownAddReminderFeature = false,
    this.shownShareAppFeature = false,
  });

  Map<String, Object> toJson() {
    return {
      "nrRemindersAdded": nrRemindersAdded,
      "nrTimesShared": nrTimesShared,
      "shownAddReminderFeature": shownAddReminderFeature,
      "shownShareAppFeature": shownShareAppFeature,
    };
  }

  @override
  String toString() {
    return '''PrefsEntity{nrRemindersAdded: $nrRemindersAdded, nrTimesShared: $nrTimesShared, 
    shownAddReminderFeature: $shownAddReminderFeature, shownShareAppFeature: $shownShareAppFeature}''';
  }

  static PrefsEntity fromJson(Map<String, Object> json) {
    return PrefsEntity(
      nrRemindersAdded: json["nrRemindersAdded"] as int,
      nrTimesShared: json["nrTimesShared"] as int,
      shownAddReminderFeature: json["shownAddReminderFeature"] as bool,
      shownShareAppFeature: json["shownShareAppFeature"] as bool,
    );
  }
}

class Prefs {
  static final FileStorage _fileStorage = FileStorage();
  static PrefsEntity _prefsCache;

  static bool get isLoading => _prefsCache == null;

  static _updatePrefs(PrefsEntity prefs) async {
    _fileStorage.savePrefs(prefs);
  }

  static Future<PrefsEntity> getPrefs() async {
    if (isLoading) {
      // await Future.delayed(Duration(seconds: 1), () {});
      _prefsCache = await _fileStorage.loadPrefs();
    }
    return _prefsCache;
  }

  static incrementNrRemindersAdded() {
    _prefsCache.nrRemindersAdded++;
    _updatePrefs(_prefsCache);
  }

  static incrementNrTimesShared() {
    _prefsCache.nrTimesShared++;
    _updatePrefs(_prefsCache);
  }

  static getShownAddReminderFeature() {
    return _prefsCache.shownAddReminderFeature ?? false;
  }

  static setShownAddReminderFeature(bool shownAddReminderFeature) {
    _prefsCache.shownAddReminderFeature = shownAddReminderFeature;
    _updatePrefs(_prefsCache);
  }

  static getShownShareAppFeature() {
    return _prefsCache.shownShareAppFeature ?? false;
  }

  static setShownShareAppFeature(bool shownShareAppFeature) {
    _prefsCache.shownShareAppFeature = shownShareAppFeature;
    _updatePrefs(_prefsCache);
  }
}
