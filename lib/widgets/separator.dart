import 'package:flutter/material.dart';

class Separator extends StatelessWidget {
  final bool dark;
  const Separator({Key key, this.dark}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 8.0),
        height: 2.0,
        width: 18.0,
        color: dark != null && dark ? Color(0xFF757575) : Colors.white);
  }
}
