import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:my_car/localization.dart';

const double _kPickerSheetHeight = 216.0;
//const double _kPickerItemHeight = 32.0;

class DatePicker extends StatefulWidget {
  final String date;
  final ValueChanged<DateTime> selectDate;

  DatePicker({Key key, this.date, this.selectDate}) : super(key: key);

  @override
  _DatePickerState createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePicker> {
  DateTime date = DateTime.now();

  Widget _buildBottomPicker(Widget picker) {
    return Container(
      height: _kPickerSheetHeight,
      padding: EdgeInsets.only(top: 6.0),
      color: CupertinoColors.white,
      child: DefaultTextStyle(
        style: TextStyle(
          color: CupertinoColors.black,
          fontSize: 22.0,
        ),
        child: GestureDetector(
          // Blocks taps from propagating to the modal sheet and popping.
          onTap: () {},
          child: SafeArea(
            top: false,
            child: picker,
          ),
        ),
      ),
    );
  }

  Widget _buildMenu(List<Widget> children) {
    return Container(
      decoration: BoxDecoration(
        // color: CupertinoColors.black,
        border: Border(
          // top: BorderSide(color: Color(0xFFBCBBC1), width: 0.0),
          bottom: BorderSide(color: Color(0xFF757575), width: 1.0),
        ),
      ),
      height: 44.0,
      child: SafeArea(
        top: false,
        bottom: false,
        child: DefaultTextStyle(
          style: TextStyle(
            letterSpacing: -0.24,
            fontSize: 17.0,
            color: Color(0xFF757575),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: children,
          ),
        ),
      ),
    );
  }

  Widget _buildDatePicker(BuildContext context) {
    return GestureDetector(
      onTap: _openDatePicker,
      child: _buildMenu(<Widget>[
        Text(RemindersLocalizations.of(context).expiresIn),
        Text(
          DateFormat('d MMM y').format(date),
          style: TextStyle(color: Color(0xFF757575)),
        ),
      ]),
    );
  }

  void _openDatePicker() {
    showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) {
        return _buildBottomPicker(
          CupertinoDatePicker(
            mode: CupertinoDatePickerMode.date,
            initialDateTime: date.isBefore(DateTime.now()) ? DateTime.now() : date,
            minimumYear: DateTime.now().year,
            maximumYear: DateTime.now().year + 10,
            onDateTimeChanged: (DateTime newDateTime) {
              setState(() {
                date = newDateTime;
                widget.selectDate(newDateTime);
              });
            },
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildDatePicker(context);
  }

  @override
  void initState() {
    super.initState();
    date = (widget.date != null ? DateTime.parse(widget.date) : DateTime.now());
  }
}
