// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ro locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'ro';

  static m0(vehicle, item) => "Atenție! \"${item}\" pentru \"${vehicle}\" expiră în două săptămâni!";

  static m1(vehicle, item) => "Atenție! \"${item}\" pentru \"${vehicle}\" expiră mâine!";

  static m2(vehicle, item) => "Atenție! \"${item}\" pentru \"${vehicle}\" expiră peste lună!";

  static m3(vehicle, item) => "Atenție! \"${item}\" pentru \"${vehicle}\" expiră peste o săptămână!";

  static m4(vehicle, item) => "Atenție! \"${item}\" pentru \"${vehicle}\" a expirat! Ferește-te de poliție!";

  static m5(vehicle) => "Ai șters \"${vehicle}\"";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "addReminder" : MessageLookupByLibrary.simpleMessage("Adaugă memento"),
    "appTitle" : MessageLookupByLibrary.simpleMessage("Mașina Mea - Mementouri"),
    "cancel" : MessageLookupByLibrary.simpleMessage("Anulare"),
    "delete" : MessageLookupByLibrary.simpleMessage("Șterge"),
    "deleteReminderConfirmation" : MessageLookupByLibrary.simpleMessage("Ștergeți acest memento?"),
    "editReminder" : MessageLookupByLibrary.simpleMessage("Editează memento"),
    "emptyItemError" : MessageLookupByLibrary.simpleMessage("Ce anume expiră?"),
    "emptyVehicleError" : MessageLookupByLibrary.simpleMessage("Despre ce mașină vreți să vă amintim?"),
    "expiresIn" : MessageLookupByLibrary.simpleMessage("Expiră în"),
    "featureAddReminderText" : MessageLookupByLibrary.simpleMessage("Apasă aici pentru a adăuga primul tău memento, e atât de simplu!"),
    "featureAddReminderTitle" : MessageLookupByLibrary.simpleMessage("Adaugă un memento nou!"),
    "featureShareText" : MessageLookupByLibrary.simpleMessage("Cu toții avem acel prieten care nu ține minte nimic. Ajută-l tu, trimite-i un link cu aplicația!"),
    "featureShareTitle" : MessageLookupByLibrary.simpleMessage("Ajută un prieten uituc!"),
    "itemHint" : MessageLookupByLibrary.simpleMessage("Mementoul"),
    "noRemindersText" : MessageLookupByLibrary.simpleMessage("Adăugă un memento nou apăsând \npe butonul + în colțul din dreapta!"),
    "noRemindersTitle" : MessageLookupByLibrary.simpleMessage("Niciun memento adăugat!"),
    "notifExpires14Days" : m0,
    "notifExpires1Day" : m1,
    "notifExpires30Days" : m2,
    "notifExpires7Days" : m3,
    "notifExpiresDanger" : MessageLookupByLibrary.simpleMessage("Atenție, ultimul avertisment!"),
    "notifExpiresToday" : m4,
    "notifExpiresWarning" : MessageLookupByLibrary.simpleMessage("Atenție!"),
    "notifsDisabledText" : MessageLookupByLibrary.simpleMessage("Vă rugăm activați notificările\ndin setările aplicației!"),
    "notifsDisabledTitle" : MessageLookupByLibrary.simpleMessage("Notificări dezactivate!"),
    "randomItem1" : MessageLookupByLibrary.simpleMessage("Asigurare"),
    "randomItem2" : MessageLookupByLibrary.simpleMessage("Cauciucuri de iarnă"),
    "randomItem3" : MessageLookupByLibrary.simpleMessage("Schimbul de ulei"),
    "randomVehicle1" : MessageLookupByLibrary.simpleMessage("Lamborghini Huracán"),
    "randomVehicle2" : MessageLookupByLibrary.simpleMessage("Porsche 911 GT3 RS"),
    "randomVehicle3" : MessageLookupByLibrary.simpleMessage("Ferrari 812 Superfast"),
    "reminderDeleted" : m5,
    "reminders" : MessageLookupByLibrary.simpleMessage("Mementouri"),
    "shareText" : MessageLookupByLibrary.simpleMessage("Nu mai uita niciodată de întreținerea sau documentele mașinii tale, instalează-ți \"Mașina Mea - Mementouri\", disponibilă pentru Android si iPhone:\n\nhttps://mycar.asistcar.ro"),
    "undo" : MessageLookupByLibrary.simpleMessage("Anulează"),
    "vehicleHint" : MessageLookupByLibrary.simpleMessage("Mașina")
  };
}
