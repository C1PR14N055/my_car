// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'en';

  static m0(vehicle, item) => "Heads up! \"${item}\" for \"${vehicle}\" expires in two weeks!";

  static m1(vehicle, item) => "Heads up! \"${item}\" for \"${vehicle}\" expires tomorrow!";

  static m2(vehicle, item) => "Heads up! \"${item}\" for \"${vehicle}\" expires in one month!";

  static m3(vehicle, item) => "Heads up! \"${item}\" for \"${vehicle}\" expires in one week!";

  static m4(vehicle, item) => "Heads up! \"${item}\" for \"${vehicle}\" has expired! Watch out for the police!";

  static m5(vehicle) => "Deleted \"${vehicle}\"";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "addReminder" : MessageLookupByLibrary.simpleMessage("Add Reminder"),
    "appTitle" : MessageLookupByLibrary.simpleMessage("My Car - Reminders"),
    "cancel" : MessageLookupByLibrary.simpleMessage("Cancel"),
    "delete" : MessageLookupByLibrary.simpleMessage("Delete"),
    "deleteReminderConfirmation" : MessageLookupByLibrary.simpleMessage("Delete this reminder?"),
    "editReminder" : MessageLookupByLibrary.simpleMessage("Edit Reminder"),
    "emptyItemError" : MessageLookupByLibrary.simpleMessage("What\'s exactly the thing that expires?"),
    "emptyVehicleError" : MessageLookupByLibrary.simpleMessage("Which car do you want to be reminded about?"),
    "expiresIn" : MessageLookupByLibrary.simpleMessage("Expires in"),
    "featureAddReminderText" : MessageLookupByLibrary.simpleMessage("Click here to add your first reminder, it\'s that easy!"),
    "featureAddReminderTitle" : MessageLookupByLibrary.simpleMessage("Add a new reminder!"),
    "featureShareText" : MessageLookupByLibrary.simpleMessage("We all have that one friend who always forgets, help him out by sending him a link to the app!"),
    "featureShareTitle" : MessageLookupByLibrary.simpleMessage("Help a forgetful friend!"),
    "itemHint" : MessageLookupByLibrary.simpleMessage("The reminder"),
    "noRemindersText" : MessageLookupByLibrary.simpleMessage("Add a new reminder by clicking\n the + button down in the right corner!"),
    "noRemindersTitle" : MessageLookupByLibrary.simpleMessage("No reminders added!"),
    "notifExpires14Days" : m0,
    "notifExpires1Day" : m1,
    "notifExpires30Days" : m2,
    "notifExpires7Days" : m3,
    "notifExpiresDanger" : MessageLookupByLibrary.simpleMessage("Atention, last warning!"),
    "notifExpiresToday" : m4,
    "notifExpiresWarning" : MessageLookupByLibrary.simpleMessage("Atention!"),
    "notifsDisabledText" : MessageLookupByLibrary.simpleMessage("Please enable notifications from\nthe app\'s settings menu!"),
    "notifsDisabledTitle" : MessageLookupByLibrary.simpleMessage("Notifications disabled!"),
    "randomItem1" : MessageLookupByLibrary.simpleMessage("Insurance"),
    "randomItem2" : MessageLookupByLibrary.simpleMessage("Winter tires"),
    "randomItem3" : MessageLookupByLibrary.simpleMessage("Oil change"),
    "randomVehicle1" : MessageLookupByLibrary.simpleMessage("Lamborghini Huracán"),
    "randomVehicle2" : MessageLookupByLibrary.simpleMessage("Porsche 911 GT3 RS"),
    "randomVehicle3" : MessageLookupByLibrary.simpleMessage("Ferrari 812 Superfast"),
    "reminderDeleted" : m5,
    "reminders" : MessageLookupByLibrary.simpleMessage("Reminders"),
    "shareText" : MessageLookupByLibrary.simpleMessage("Never forget about your car\'s maintenance or documents, check out \"My Car - Reminders\", available for Android and iPhone:\n\nhttps://mycar.asistcar.ro"),
    "undo" : MessageLookupByLibrary.simpleMessage("Undo"),
    "vehicleHint" : MessageLookupByLibrary.simpleMessage("The car")
  };
}
