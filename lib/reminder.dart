import 'package:my_car/uuid.dart';

class ReminderEntity {
  final String id;
  final String vehicle;
  final String item;
  final String expires;

  ReminderEntity(this.id, this.vehicle, this.item, this.expires);

  @override
  int get hashCode =>
      id.hashCode ^ vehicle.hashCode ^ item.hashCode ^ expires.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ReminderEntity &&
          runtimeType == other.runtimeType &&
          expires == other.expires &&
          item == other.item &&
          vehicle == other.vehicle &&
          id == other.id;

  Map<String, Object> toJson() {
    return {
      "id": id,
      "vehicle": vehicle,
      "item": item,
      "expires": expires,
    };
  }

  @override
  String toString() {
    return 'ReminderEntity{expires: $expires, item: $item, vehicle: $vehicle, id: $id}';
  }

  static ReminderEntity fromJson(Map<String, Object> json) {
    return ReminderEntity(
      json["id"] as String,
      json["vehicle"] as String,
      json["item"] as String,
      json["expires"] as String,
    );
  }
}

class Reminder {
  final String id;
  final String vehicle;
  final String item;
  final String expires;

  Reminder({this.vehicle, this.item, this.expires, String id})
      : this.id = id ?? Uuid().generateV4();

  @override
  int get hashCode =>
      id.hashCode ^ vehicle.hashCode ^ item.hashCode ^ expires.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Reminder &&
          runtimeType == other.runtimeType &&
          expires == other.expires &&
          item == other.item &&
          vehicle == other.vehicle &&
          id == other.id;

  @override
  String toString() {
    return 'Reminder{expires: $expires, item: $item, vehicle: $vehicle, id: $id}';
  }

  ReminderEntity toEntity() {
    return ReminderEntity(id, vehicle, item, expires);
  }

  static Reminder fromEntity(ReminderEntity entity) {
    return Reminder(
        vehicle: entity.vehicle,
        id: entity.id,
        item: entity.item,
        expires: entity.expires);
  }

  Reminder copy({String expires, String item, String vehicle, String id}) {
    return Reminder(
      vehicle: vehicle ?? this.vehicle,
      expires: expires ?? this.expires,
      item: item ?? this.item,
      id: id ?? this.id,
    );
  }

  static Reminder blank() {
    DateTime _date = DateTime.now();

    return Reminder(
        vehicle: null,
        item: null,
        expires:
            DateTime(_date.year + 1, _date.month, _date.day - 1).toString());
  }
}

class ReminderUpdateAction {
  final Reminder reminder;
  final ReminderUpdateActionType type;

  ReminderUpdateAction(this.reminder, this.type);
}

enum ReminderUpdateActionType { delete, cancel, update }
