// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:my_car/prefs.dart';
import 'package:my_car/reminder.dart';
import 'package:path_provider/path_provider.dart';

/// Loads and saves a List of Reminders using a text file stored on the device.
class FileStorage {
  final Future<Directory> Function() getDirectory =
      getApplicationDocumentsDirectory;
  Future<List<ReminderEntity>> loadReminders() async {
    final file = await _getLocalRemindersFile();
    final string = await file.readAsString();
    final json = JsonDecoder().convert(string);
    // FIXME: change todos to reminders WITHOUT BREAKING productions app
    final todos = (json['todos'])
        .map<ReminderEntity>((todo) => ReminderEntity.fromJson(todo))
        .toList();

    return todos;
  }

  Future<File> saveReminders(List<ReminderEntity> todos) async {
    final file = await _getLocalRemindersFile();

    return file.writeAsString(JsonEncoder().convert({
      'todos': todos.map((todo) => todo.toJson()).toList(),
    }));
  }

  Future<File> _getLocalRemindersFile() async {
    final dir = await getDirectory();

    return File('${dir.path}/my_car_reminders_storage.json');
  }

  Future<PrefsEntity> loadPrefs() async {
    //SettingsEntity
    try {
      final file = await _getLocalPrefsFile();
      final string = await file.readAsString();
      final json = JsonDecoder().convert(string);

      return PrefsEntity.fromJson(json['prefs']);
    } catch (e) {
      return PrefsEntity();
    }
  }

  Future<File> savePrefs(PrefsEntity prefs) async {
    final file = await _getLocalPrefsFile();

    return file.writeAsString(JsonEncoder().convert({
      'prefs': prefs.toJson(),
    }));
  }

  Future<File> _getLocalPrefsFile() async {
    final dir = await getDirectory();

    return File('${dir.path}/my_car_prefs_storage.json');
  }

  Future<void> clean() async {
    final remindersFile = await _getLocalRemindersFile();
    final prefsFile = await _getLocalPrefsFile();

    remindersFile.delete();
    prefsFile.delete();
  }
}
