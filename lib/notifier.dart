import 'package:flutter/widgets.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:my_car/localization.dart';
import 'package:my_car/reminder.dart';

class Notifier {
  static FlutterLocalNotificationsPlugin _flnp;
  static AndroidInitializationSettings _ais;
  static IOSInitializationSettings _iis;
  static InitializationSettings _is;

  static AndroidNotificationDetails _and;
  static IOSNotificationDetails _ind;
  static NotificationDetails _nd;

  static RemindersLocalizations localizations;

  static Future<bool> init(BuildContext context) async {
    localizations = RemindersLocalizations.of(context);
    _flnp = new FlutterLocalNotificationsPlugin();
    _ais = AndroidInitializationSettings('@mipmap/ic_launcher');
    _iis = IOSInitializationSettings(
      requestAlertPermission: true,
      requestBadgePermission: true,
      requestSoundPermission: true,
    );
    _is = InitializationSettings(_ais, _iis);

    bool permission = await _flnp.initialize(_is);

    // TODO: custom notif for android
    _and = new AndroidNotificationDetails(
      'my_car_reminders_id',
      'my_car_reminders_name',
      'my_car_reminders_description',
      importance: Importance.Max,
      priority: Priority.Max,
    );
    _ind = new IOSNotificationDetails(
      presentAlert: true,
      presentBadge: true,
      presentSound: true,
    );
    _nd = new NotificationDetails(_and, _ind);

    print('init notif permission: $permission');

    return permission;
  }

  void _showNotif(int id, String title, String body,
      {String payload}) async {
    await _flnp.show(
      id,
      title,
      body,
      _nd,
      payload: payload,
    );
  }

  static void scheduleNotif(int id, String title, String body, String notifDate,
      {String payload, int substractDays}) async {
    DateTime notifDateTime = DateTime.parse(notifDate);
    notifDateTime = DateTime(
        notifDateTime.year, notifDateTime.month, notifDateTime.day, 9, 0);
    if (substractDays != null) {
      print(notifDateTime);
      notifDateTime = notifDateTime.subtract(Duration(days: substractDays));
      print('$notifDateTime - $substractDays');
    }
    await _flnp.schedule(id, title, body, notifDateTime, _nd,
        payload: 'payload');
  }

  static void scheduleAll(List reminders) async {
    cancelAll();
    for (int i = 0; i < reminders.length * 5; i += 5) {
      Reminder reminder = reminders[i ~/ 5];
      DateTime expires = DateTime.parse(reminder.expires);
      DateTime today = DateTime.now();
      today = DateTime(today.year, today.month, today.day, 0, 0);

      // print('${i ~/ 5} -> $i');
      // print('${i ~/ 5} -> ${i + 1}');
      // print('${i ~/ 5} -> ${i + 2}');
      // print('${i ~/ 5} -> ${i + 3}');
      // print('${i ~/ 5} -> ${i + 4}');
      // print(r.expires);
      // continue;
      if (expires
          .subtract(Duration(days: 30))
          .isAfter(today.add(Duration(days: 1)))) {
        print('isAfter > 30');
        scheduleNotif(
          i,
          localizations.notifExpiresWarning,
          localizations.notifExpires30Days(reminder.vehicle, reminder.item),
          reminder.expires,
          substractDays: 30,
        );
      }
      if (expires
          .subtract(Duration(days: 14))
          .isAfter(today.add(Duration(days: 1)))) {
        print('isAfter > 14');
        scheduleNotif(
          i + 1,
          localizations.notifExpiresWarning,
          localizations.notifExpires14Days(reminder.vehicle, reminder.item),
          reminder.expires,
          substractDays: 14,
        );
      }
      if (expires
          .subtract(Duration(days: 7))
          .isAfter(today.add(Duration(days: 1)))) {
        print('isAfter > 7');
        scheduleNotif(
          i + 2,
          localizations.notifExpiresWarning,
          localizations.notifExpires7Days(reminder.vehicle, reminder.item),
          reminder.expires,
          substractDays: 7,
        );
      }
      if (expires.isAfter(today.add(Duration(days: 1)))) {
        print('isAfter >= 1');
        scheduleNotif(
          i + 3,
          localizations.notifExpiresWarning,
          localizations.notifExpires1Day(reminder.vehicle, reminder.item),
          reminder.expires,
          substractDays: 1,
        );
      }
      if (expires.isAfter(today.add(Duration(days: 1))) ||
          expires.compareTo(today.add(Duration(days: 1))) == 0) {
        print('is == 1');
        scheduleNotif(
          i + 4,
          localizations.notifExpiresDanger,
          localizations.notifExpiresToday(reminder.vehicle, reminder.item),
          reminder.expires,
        );
      }
    }
  }

  static void cancelAll() async {
    await _flnp.cancelAll();
  }
}
