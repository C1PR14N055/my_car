// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'package:flutter/widgets.dart';

class RemindersKeys {
  // Home Screens
  static final homeScreen = const Key('__homeScreen__');
  static final addReminderFab = const Key('__addReminderFab__');
  static final snackbar = const Key('__snackbar__');
  static Key snackbarAction(String id) => Key('__snackbar_action_${id}__');

  // Reminders
  static final reminderList = const Key('__reminderList__');
  static final remindersLoading = const Key('__remindersLoading__');
  static final reminderItem = (String id) => Key('ReminderItem__${id}');
  static final reminderItemCheckbox =
      (String id) => Key('ReminderItem__${id}__Checkbox');
  static final reminderItemVehicle = (String id) => Key('ReminderItem__${id}__Vehicle');
  static final reminderItemItem = (String id) => Key('ReminderItem__${id}__Item');
  static final reminderItemExpires = (String id) => Key('ReminderItem__${id}__Expires');

  // Tabs
  static final tabs = const Key('__tabs__');
  static final reminderTab = const Key('__reminderTab__');
  static final statsTab = const Key('__statsTab__');

  // Extra Actions
  static final extraActionsButton = const Key('__extraActionsButton__');
  static final toggleAll = const Key('__markAllDone__');
  static final clearCompleted = const Key('__clearCompleted__');

  // Filters
  static final filterButton = const Key('__filterButton__');
  static final allFilter = const Key('__allFilter__');
  static final activeFilter = const Key('__activeFilter__');
  static final completedFilter = const Key('__completedFilter__');

  // Add Screen
  static final addReminderScreen = const Key('__addReminderScreen__');
  static final saveNewReminder = const Key('__saveNewReminder__');
  static final vehicleField = const Key('__vehicleField__');
  static final itemField = const Key('__itemField__');
  static final expiresField = const Key('__expiresField__');

  // Edit Screen
  static final editReminderScreen = const Key('__editReminderScreen__');
  static final saveReminderFab = const Key('__saveReminderFab__');
}
