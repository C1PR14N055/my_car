import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:my_car/widgets/feature_discovery.dart';
import 'package:flutter/widgets.dart';
import 'package:my_car/localization.dart';
import 'package:my_car/reminder_model.dart';
import 'package:my_car/reminders_theme_style.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:my_car/screens/home_screen.dart';
import 'package:scoped_model/scoped_model.dart';

void main() {
  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    // timeDilation = 10;
    // debugPaintSizeEnabled = true;
    // debugRepaintRainbowEnabled = true;
    // debugRepaintTextRainbowEnabled = true;
    return Container(
      child: FeatureDiscovery(
        child: ScopedModel<ReminderListModel>(
          model: ReminderListModel(),
          child: MaterialApp(
            builder: (context, child) {
              // dissables font scaling
              return MediaQuery(
                child: child,
                data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
              );
            },
            title: 'My Car - Reminders',
            theme: RemindersTheme.theme,
            debugShowCheckedModeBanner: false,
            localizationsDelegates: [
              RemindersLocalizationsDelegate(),
              FallbackCupertinoLocalisationsDelegate(),
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
            ],
            supportedLocales: [const Locale('en', ''), const Locale('ro', '')],
            home: HomeScreen(),
          ),
        ),
      ),
    );
  }
}
