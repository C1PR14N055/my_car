import 'package:share/share.dart';
import 'package:my_car/localization.dart';

class Sharer {
  static void share(context) {
    RemindersLocalizations localizations = RemindersLocalizations.of(context);
    Share.share(localizations.shareText);
  }
}