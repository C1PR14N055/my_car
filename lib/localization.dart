// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

// Generate arb files:
// flutter pub pub run intl_translation:extract_to_arb --output-dir=lib/l10n lib/localization.dart
// Copy strings and delete intl_messages.arb
// Generate dart files from arb:
// flutter pub pub run intl_translation:generate_from_arb --output-dir=lib/l10n --no-use-deferred-loading lib/localization.dart lib/l10n/intl_*.arb

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

import 'package:my_car/l10n/messages_all.dart';

class RemindersLocalizations {
  static Future<RemindersLocalizations> load(Locale locale) {
    final String localeName = Intl.canonicalizedLocale(
      locale.countryCode.isEmpty ? locale.languageCode : locale.toString(),
    );

    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return RemindersLocalizations();
    });
  }

  static RemindersLocalizations of(BuildContext context) {
    return Localizations.of<RemindersLocalizations>(
        context, RemindersLocalizations);
  }

  String get appTitle => Intl.message(
        'My Car - Reminders',
        name: 'appTitle',
        args: [],
      );

  String get reminders => Intl.message(
        'Reminders',
        name: 'reminders',
        args: [],
      );

  String get vehicleHint => Intl.message(
        'The car',
        name: 'vehicleHint',
        args: [],
      );

  String get emptyVehicleError => Intl.message(
        'Which car do you want to be reminded about?',
        name: 'emptyVehicleError',
        args: [],
      );

  String get itemHint => Intl.message(
        'The reminder',
        name: 'itemHint',
        args: [],
      );

  String get emptyItemError => Intl.message(
        'What\'s exactly the thing that expires?',
        name: 'emptyItemError',
        args: [],
      );

  String get expiresIn => Intl.message(
        'Expires in',
        name: 'expiresIn',
        args: [],
      );

  String get addReminder => Intl.message(
        'Add Reminder',
        name: 'addReminder',
        args: [],
      );

  String get editReminder => Intl.message(
        'Edit Reminder',
        name: 'editReminder',
        args: [],
      );

  String reminderDeleted(String vehicle) => Intl.message(
        'Deleted "$vehicle"',
        name: 'reminderDeleted',
        args: [vehicle],
      );

  String get undo => Intl.message(
        'Undo',
        name: 'undo',
        args: [],
      );

  String get deleteReminderConfirmation => Intl.message(
        'Delete this reminder?',
        name: 'deleteReminderConfirmation',
        args: [],
      );

  String get delete => Intl.message(
        'Delete',
        name: 'delete',
        args: [],
      );

  String get cancel => Intl.message(
        'Cancel',
        name: 'cancel',
        args: [],
      );

  String get shareText => Intl.message(
        '''Never forget about your car's maintenance or documents, check out \"My Car - Reminders\", available for Android and iPhone:
        
        https://mycar.asistcar.ro
        ''',
        name: 'shareText',
        args: [],
      );

  String get randomVehicle1 => Intl.message(
        'Lamborghini Huracán',
        name: 'randomVehicle1',
        args: [],
      );

  String get randomVehicle2 => Intl.message(
        'Porsche 911 GT3 RS',
        name: 'randomVehicle2',
        args: [],
      );

  String get randomVehicle3 => Intl.message(
        'Ferrari 812 Superfast',
        name: 'randomVehicle3',
        args: [],
      );

  String get randomItem1 => Intl.message(
        'Insurance',
        name: 'randomItem1',
        args: [],
      );

  String get randomItem2 => Intl.message(
        'Winter tires',
        name: 'randomItem2',
        args: [],
      );
  String get randomItem3 => Intl.message(
        'Oil change',
        name: 'randomItem3',
        args: [],
      );

  String get noRemindersTitle => Intl.message(
        'No reminders added!',
        name: 'noRemindersTitle',
        args: [],
      );

  String get noRemindersText => Intl.message(
        'Add a new reminder by clicking\n the + button down in the right corner!',
        name: 'noRemindersText',
        args: [],
      );

  String get notifExpiresWarning => Intl.message(
        'Atention!',
        name: 'notifExpiresWarning',
        args: [],
      );

  String get notifExpiresDanger => Intl.message(
        'Atention, last warning!',
        name: 'notifExpiresDanger',
        args: [],
      );

  String notifExpires30Days(String vehicle, String item) => Intl.message(
        'Heads up! "$item" for "$vehicle" expires in one month!',
        name: 'notifExpires30Days',
        args: [vehicle, item],
      );

  String notifExpires14Days(String vehicle, String item) => Intl.message(
        'Heads up! "$item" for "$vehicle" expires in two weeks!',
        name: 'notifExpires14Days',
        args: [vehicle, item],
      );

  String notifExpires7Days(String vehicle, String item) => Intl.message(
        'Heads up! "$item" for "$vehicle" expires in one week!',
        name: 'notifExpires7Days',
        args: [vehicle, item],
      );

  String notifExpires1Day(String vehicle, String item) => Intl.message(
        'Heads up! "$item" for "$vehicle" expires tomorrow!',
        name: 'notifExpires1Day',
        args: [vehicle, item],
      );

  String notifExpiresToday(String vehicle, String item) => Intl.message(
        'Heads up! "$item" for "$vehicle" has expired! Watch out for the police if they stop you!',
        name: 'notifExpiresToday',
        args: [vehicle, item],
      );

  String get featureShareTitle => Intl.message(
        'Help a forgetful friend!',
        name: 'featureShareTitle',
        args: [],
      );

  String get featureShareText => Intl.message(
        'We all have that one friend who always forgets, help him out by sending him a link to the app!',
        name: 'featureShareText',
        args: [],
      );

  String get featureAddReminderTitle => Intl.message(
        'Add a new reminder!',
        name: 'featureAddReminderTitle',
        args: [],
      );

  String get featureAddReminderText => Intl.message(
        'Click here to add your first reminder, it\'s that easy!',
        name: 'featureAddReminderText',
        args: [],
      );

  String get notifsDisabledTitle => Intl.message(
        'Notifications disabled!',
        name: 'notifsDisabledTitle',
        args: [],
      );

  String get notifsDisabledText => Intl.message(
        'Please enable notifications from\nthe app\'s settings menu!',
        name: 'notifsDisabledText',
        args: [],
      );
}

class RemindersLocalizationsDelegate
    extends LocalizationsDelegate<RemindersLocalizations> {
  @override
  bool isSupported(Locale locale) =>
      ['en', 'ro'].contains(locale.languageCode.toLowerCase());

  @override
  Future<RemindersLocalizations> load(Locale locale) =>
      RemindersLocalizations.load(locale);

  @override
  bool shouldReload(RemindersLocalizationsDelegate old) => false;
}

// TODO: Remove after flutter ads cupertino locales
class FallbackCupertinoLocalisationsDelegate
    extends LocalizationsDelegate<CupertinoLocalizations> {
  const FallbackCupertinoLocalisationsDelegate();

  @override
  bool isSupported(Locale locale) => true;

  @override
  Future<CupertinoLocalizations> load(Locale locale) =>
      DefaultCupertinoLocalizations.load(locale);

  @override
  bool shouldReload(FallbackCupertinoLocalisationsDelegate old) => false;
}
