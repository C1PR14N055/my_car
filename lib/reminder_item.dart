import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:my_car/localization.dart';
import 'package:my_car/reminder.dart';
import 'package:my_car/reminders_theme_style.dart';
import 'package:my_car/widgets/separator.dart';

class ReminderPreview extends StatelessWidget {
  final Reminder reminder;
  final bool horizontal;

  ReminderPreview(this.reminder, {this.horizontal = true});
  ReminderPreview.vertical(this.reminder) : horizontal = false;

  @override
  Widget build(BuildContext context) {
    RemindersLocalizations localizations = RemindersLocalizations.of(context);
    String _randomVehicle = [
      localizations.randomVehicle1,
      localizations.randomVehicle3,
      localizations.randomVehicle2
    ][new Random().nextInt(2)];

    String _randomItem = [
      localizations.randomItem1,
      localizations.randomItem3,
      localizations.randomItem2
    ][new Random().nextInt(2)];

    List<Color> _getGradient(String expires) {
      DateTime expiryDate = DateTime.parse(expires);
      if (expiryDate.difference(DateTime.now()).inDays <= 0) {
        return [
          Color.fromARGB(255, 244, 67, 54),
          Color.fromARGB(170, 244, 67, 54),
          Color.fromARGB(127, 244, 67, 54),
        ];
      }
      if (expiryDate.difference(DateTime.now()).inDays < 30) {
        return [
          Color.fromARGB(255, 255, 193, 7),
          Color.fromARGB(170, 255, 193, 7),
          Color.fromARGB(127, 255, 193, 7),
        ];
      }
      // Default case => ok
      return [
        Color.fromARGB(255, 76, 175, 80),
        Color.fromARGB(170, 76, 175, 80),
        Color.fromARGB(127, 76, 175, 80),
      ];
    }

    Widget _getImageIcon(String expires) {
      DateTime expiryDate = DateTime.parse(expires);
      if (expiryDate.difference(DateTime.now()).inDays <= 0) {
        return horizontal
          ? Image.asset('assets/imgs/octogon_half.png', width: 92, height: 92)
          : Image.asset('assets/imgs/octogon_full.png', width: 92, height: 92);
      }
      if (expiryDate.difference(DateTime.now()).inDays < 30) {
        return horizontal
          ? Image.asset('assets/imgs/triangle_half.png', width: 92, height: 92)
          : Image.asset('assets/imgs/triangle_full.png', width: 92, height: 92);
      }
      // Default case => ok
      return horizontal
          ? Image.asset('assets/imgs/circle_half.png', width: 92, height: 92)
          : Image.asset('assets/imgs/circle_full.png', width: 92, height: 92);
    }

    final reminderThumbnail = Container(
      margin: horizontal
          ? EdgeInsets.symmetric(vertical: 16.0)
          : EdgeInsets.symmetric(vertical: 18.0),
      alignment:
          horizontal ? FractionalOffset.centerLeft : FractionalOffset.center,
      child: Hero(
        tag: "reminder-hero-${reminder.id}",
        child: _getImageIcon(reminder?.expires),
        flightShuttleBuilder: (
          BuildContext flightContext,
          Animation<double> animation,
          HeroFlightDirection flightDirection,
          BuildContext fromHeroContext,
          BuildContext toHeroContext,
        ) {
          final Hero fromHero = fromHeroContext.widget;
          final Hero toHero = toHeroContext.widget;

          return RotationTransition(
            turns: animation,
            child: Stack(
              children: <Widget>[
                flightDirection == HeroFlightDirection.push ? fromHero : toHero,
                FadeTransition(
                  opacity: animation,
                  child: flightDirection == HeroFlightDirection.push
                      ? toHero
                      : fromHero,
                ),
              ],
            ),
          );
        },
      ),
    );

    Widget _reminderValue({String value}) {
      return Container(
        child: Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
          Icon(
            Icons.calendar_today,
            size: 14.0,
            color: Colors.white,
          ),
          SizedBox(width: 8.0),
          FittedBox(
            child: Text(value, style: RemindersStyle.smallTextStyle),
            fit: BoxFit.scaleDown,
          )
        ]),
      );
    }

    final reminderCardContent = Container(
      margin: EdgeInsets.fromLTRB(
        horizontal ? 72.0 : 16.0,
        horizontal ? 12.0 : 38.0,
        16.0,
        16.0,
      ),
      constraints: BoxConstraints.expand(),
      child: Column(
        crossAxisAlignment:
            horizontal ? CrossAxisAlignment.start : CrossAxisAlignment.center,
        children: <Widget>[
          horizontal ? SizedBox(height: 2.0) : SizedBox(height: 8.0),
          FittedBox(
            child: Text(
              reminder.vehicle != null && reminder.vehicle.isNotEmpty
                  ? reminder.vehicle
                  : _randomVehicle,
              style: RemindersStyle.titleTextStyle,
              overflow: TextOverflow.ellipsis,
            ),
            fit: BoxFit.scaleDown,
          ),
          FittedBox(
            child: Text(
              reminder.item != null && reminder.item.isNotEmpty
                  ? reminder.item
                  : _randomItem,
              style: RemindersStyle.commonTextStyle,
              overflow: TextOverflow.ellipsis,
            ),
            fit: BoxFit.scaleDown,
          ),
          SizedBox(height: 8.0),
          Separator(),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                  flex: horizontal ? 1 : 0,
                  child: _reminderValue(
                    value: DateFormat('d MMM y')
                        .format(DateTime.parse(reminder.expires)),
                  )),
            ],
          ),
        ],
      ),
    );

    final reminderCard = Container(
      child: reminderCardContent,
      height: horizontal ? 124.0 : 154.0,
      margin:
          horizontal ? EdgeInsets.only(left: 46.0) : EdgeInsets.only(top: 72.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          // Where the linear gradient begins and ends
          begin: Alignment.bottomLeft,
          end: Alignment.topRight,
          // Add one stop for each color. Stops should increase from 0 to 1
          stops: [0, 0.75, 1],
          colors: _getGradient(reminder.expires),
        ),
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(8.0),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black12,
            blurRadius: 10.0,
            offset: Offset(0.0, 10.0),
          ),
        ],
      ),
    );

    return Container(
      margin: EdgeInsets.symmetric(
        vertical: 8.0,
        horizontal: 16.0,
      ),
      child: Stack(
        children: <Widget>[
          reminderCard,
          reminderThumbnail,
        ],
      ),
    );
  }
}
