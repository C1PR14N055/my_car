import 'package:flutter/widgets.dart';
import 'package:my_car/file_storage.dart';
import 'package:my_car/reminder.dart';
// import 'package:my_car/reminders_repository.dart';

import 'package:scoped_model/scoped_model.dart';

class ReminderListModel extends Model {
  final FileStorage fileStorage = FileStorage();
  List<Reminder> get reminders => _reminders.toList();
  List<Reminder> _reminders = [];
  bool isLoading = true;
  bool get isEmpty => _reminders.length <= 0;

  /// Wraps [ScopedModel.of] for this [Model]. See [ScopedModel.of] for more
  static ReminderListModel of(BuildContext context) =>
      ScopedModel.of<ReminderListModel>(context);

  @override
  void addListener(VoidCallback listener) {
    super.addListener(listener);
    // update data for every subscriber, especially for the first one
    loadReminders();
  }

  /// Loads remote data
  ///
  /// Call this initially and when the user manually refreshes
  Future loadReminders() {
    isLoading = true;
    return fileStorage.loadReminders().then((loadedReminders) {
      _reminders = loadedReminders.map(Reminder.fromEntity).toList();
      isLoading = false;
      notifyListeners();
    }).catchError((err) {
      _reminders = [];
      isLoading = false;
      notifyListeners();
    });
  }

  List<Reminder> get sortedReminders {
    return _reminders
      ..sort((a, b) {
        return DateTime.parse(b.expires).isBefore(DateTime.parse(a.expires))
            ? 1
            : 0;
      });
  }

  /// updates a [Reminder] by replacing the item with the same id by the parameter [reminder]
  void updateReminder(Reminder reminder) {
    assert(reminder != null);
    assert(reminder.id != null);
    var oldReminder = _reminders.firstWhere((it) => it.id == reminder.id);
    var replaceIndex = _reminders.indexOf(oldReminder);
    _reminders.replaceRange(replaceIndex, replaceIndex + 1, [reminder]);
    notifyListeners();
    _uploadItems();
  }

  void removeReminder(Reminder reminder) {
    _reminders.removeWhere((it) => it.id == reminder.id);
    notifyListeners();
    _uploadItems();
  }

  void addReminder(Reminder reminder) {
    _reminders.add(reminder);
    notifyListeners();
    _uploadItems();
  }

  void _uploadItems() {
    fileStorage.saveReminders(_reminders.map((it) => it.toEntity()).toList());
  }

  Reminder reminderById(String id) {
    return _reminders.firstWhere((it) => it.id == id, orElse: () => null);
  }
}
