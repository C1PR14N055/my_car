import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/scheduler.dart';
import 'package:my_car/sound.dart';

enum ReminderActionButtonActions { delete, update }

class ReminderActionButton extends StatefulWidget {
  final ReminderActionButtonActions action;
  final VoidCallback onTap;
  final GlobalKey<FormState> formKey;

  const ReminderActionButton({this.action, this.onTap, this.formKey, Key key})
      : super(key: key);

  @override
  ReminderActionButtonState createState() => ReminderActionButtonState();
}

class ReminderActionButtonState extends State<ReminderActionButton>
    with TickerProviderStateMixin {
  AnimationController _controller;
  bool _enabled = true;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: Duration(milliseconds: 150),
      vsync: this,
    );
    _controller.addStatusListener((AnimationStatus status) {
      if (AnimationStatus.completed == status) {
        if (widget.action == ReminderActionButtonActions.delete) {
          Sound.playLocal('delete.mp3');
        } else if (widget.action == ReminderActionButtonActions.update) {
          Sound.playLocal('save.mp3');
        }
      }
    });
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  Future _startAnimation() async {
    try {
      await _controller.forward().orCancel;
    } on TickerCanceled {
      print('Animation Failed');
    }
  }

  @override
  Widget build(BuildContext context) {
    // timeDilation = 10.5;
    return GestureDetector(
      onTap: () {
        if (_enabled) {
          _enabled = false;
          widget.onTap();
          if (widget.formKey.currentState.validate()) {
            _startAnimation();
          } else {
            _enabled = true;
          }
        }
      },
      child: AnimatedBox(controller: _controller, action: widget.action),
    );
  }
}

class AnimatedBox extends StatelessWidget {
  AnimatedBox({Key key, this.controller, this.action})
      : size = Tween<double>(
          begin: 1.0,
          end: 5.0,
        ).animate(
          CurvedAnimation(
              parent: controller,
              curve: Interval(
                0.0,
                1.0,
                curve: Curves.easeIn,
              )),
        ),
        transX = Tween<double>(
          begin: 0.0,
          end: 1.0,
        ).animate(
          CurvedAnimation(
              parent: controller,
              curve: Interval(
                0.0,
                0.75,
                curve: Curves.easeIn,
              )),
        ),
        transY = Tween<double>(
          begin: 0.0,
          end: 1.0,
        ).animate(
          CurvedAnimation(
              parent: controller,
              curve: Interval(
                0.0,
                1.0,
                curve: Curves.easeIn,
              )),
        ),
        super(key: key);

  final ReminderActionButtonActions action;
  final Animation<double> controller;
  final Animation<double> size;
  final Animation<double> transX;
  final Animation<double> transY;

  // button / icon size
  static const double _size = 80;
  // parent container padding
  static const double _parentPadding = 32;

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;

    return AnimatedBuilder(
      animation: controller,
      builder: (BuildContext context, Widget child) {
        double tX = action == ReminderActionButtonActions.delete
            ? transX.value * (_width / 4 - _parentPadding / 2)
            : transX.value * (-_width / 4 + _parentPadding / 2);
        double tY = transY.value * (-_height / 2.5);

        return Transform.translate(
          child: Transform.scale(
            child: Container(
              alignment: Alignment.topCenter,
              width: _size,
              height: _size,
              child: Icon(
                action == ReminderActionButtonActions.delete
                    ? CupertinoIcons.clear
                    : CupertinoIcons.check_mark,
                size: _size,
                color: action == ReminderActionButtonActions.delete
                    ? Colors.red
                    : Colors.green,
              ),
            ),
            scale: size.value,
          ),
          offset: Offset(tX, tY),
        );
      },
    );
  }
}
