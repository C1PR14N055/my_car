// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_car/localization.dart';
import 'package:my_car/reminder.dart';
import 'package:my_car/reminder_item.dart';
import 'package:my_car/reminder_model.dart';
import 'package:my_car/reminders_keys.dart';
import 'package:my_car/reminders_theme_style.dart';
import 'package:my_car/screens/reminder_action_button.dart';
import 'package:my_car/widgets/date_picker.dart';
import 'package:my_car/widgets/separator.dart';
import 'package:scoped_model/scoped_model.dart';

class AddEditScreen extends StatefulWidget {
  final String reminderId;
  final VoidCallback reminderSaved;
  final VoidCallback reminderDeleted;

  AddEditScreen(
      {Key key, this.reminderId, this.reminderSaved, this.reminderDeleted})
      : super(key: key ?? RemindersKeys.addReminderScreen);
  @override
  _AddEditScreenState createState() => _AddEditScreenState();
}

class _AddEditScreenState extends State<AddEditScreen> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String _wallpaper;
  Reminder _reminder;

  bool get isEditing => widget.reminderId != null;

  final TextEditingController vehicleController = TextEditingController();
  final TextEditingController itemController = TextEditingController();

  final FocusNode _focusNodeVehicle = FocusNode();
  final FocusNode _focusNodeItem = FocusNode();

  @override
  void initState() {
    super.initState();
    int _random =
        Random().nextInt(50) + 1; // TODO: move to const... 50 wallpapers
    _wallpaper = 'img_' + '$_random'.padLeft(5, '0') + '.jpeg';
    _reminder = ReminderListModel.of(context).reminderById(widget.reminderId) ??
        Reminder.blank();

    vehicleController.text = _reminder.vehicle;
    itemController.text = _reminder.item;
    vehicleController.addListener(_updateVehicle);
    itemController.addListener(_updateItem);
  }

  @override
  void dispose() {
    super.dispose();
    vehicleController.dispose();
    itemController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // timeDilation = 5.0;
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        color: Color(0xFFFFFFFF),
        child: Stack(
          children: [
            _getBackground(),
            _getTopGradient(),
            _getBottomGradient(),
            _getContent(context),
            _getToolbar(context),
          ],
        ),
      ),
    );
  }

  Container _getBackground() {
    return Container(
      child: Image.asset(
        'assets/wallpapers/$_wallpaper',
        fit: BoxFit.cover,
        height: MediaQuery.of(context).size.height * 0.4,
      ),
      constraints: BoxConstraints.expand(
        height: MediaQuery.of(context).size.height * 0.4,
      ),
    );
  }

  Container _getTopGradient() {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: [
              Color(0x88000000),
              Color(0x00000000),
            ],
            stops: [
              0,
              0.5
            ],
            begin: FractionalOffset.topCenter,
            end: FractionalOffset.bottomCenter),
      ),
      height: MediaQuery.of(context).size.height * 0.2,
    );
  }

  Container _getBottomGradient() {
    return Container(
      margin:
          EdgeInsets.only(top: (MediaQuery.of(context).size.height * 0.3) + 1),
      height: MediaQuery.of(context).size.height * 0.1,
      decoration: BoxDecoration(
        // color: Color(0xFFFFFFFF),
        gradient: LinearGradient(
          colors: [
            Color(0x00FFFFFF),
            Color(0xFFFFFFFF),
          ],
          stops: [0.0, 1.0],
          begin: FractionalOffset.topCenter,
          end: FractionalOffset.bottomCenter,
        ),
      ),
    );
  }

  ScopedModelDescendant _getContent(context) {
    final _overviewTitle = isEditing
        ? RemindersLocalizations.of(context).editReminder.toUpperCase()
        : RemindersLocalizations.of(context).addReminder.toUpperCase();
    final localizations = RemindersLocalizations.of(context);
    final textTheme = Theme.of(context).textTheme;

    return ScopedModelDescendant<ReminderListModel>(
      builder: (BuildContext context, Widget child, ReminderListModel model) {
        return Container(
          child: ListView(
            children: <Widget>[
              ReminderPreview(
                Reminder(
                  vehicle: _reminder.vehicle,
                  item: _reminder.item,
                  expires: _reminder.expires,
                  id: _reminder.id,
                ),
                horizontal: false,
              ),
              Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                  colors: [
                    Color(0x00FFFFFF),
                    Color(0xBBFFFFFF),
                    Color(0xBBFFFFFF),
                    Color(0x00FFFFFF),
                  ],
                  stops: [0, 0.25, 0.75, 1],
                  begin: FractionalOffset.topCenter,
                  end: FractionalOffset.bottomCenter,
                )),
                padding: EdgeInsets.symmetric(horizontal: 32.0),
                height: MediaQuery.of(context).size.height * 0.6,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: 8.0,
                        ),
                        Text(
                          _overviewTitle,
                          style: RemindersStyle.headerTextStyle,
                        ),
                        Separator(dark: true),
                        SizedBox(
                          height: 8.0,
                        ),
                      ],
                    ),
                    Container(
                      child: Form(
                        key: _formKey,
                        autovalidate: true,
                        child: Column(
                          children: [
                            TextFormField(
                              key: RemindersKeys.vehicleField,
                              // autofocus: !isEditing,
                              style: textTheme.title,
                              decoration: InputDecoration(
                                hintText: localizations.vehicleHint,
                              ),
                              validator: (val) {
                                return val.trim().isEmpty
                                    ? localizations.emptyVehicleError
                                    : null;
                              },
                              controller: vehicleController,
                              textInputAction: TextInputAction.next,
                              focusNode: _focusNodeVehicle,
                              onSaved: (value) =>
                                  _reminder = _reminder.copy(vehicle: value),
                              onFieldSubmitted: (value) {
                                _focusNodeVehicle.unfocus();
                                FocusScope.of(context)
                                    .requestFocus(_focusNodeItem);
                              },
                              maxLength: 24,
                              maxLines: 1,
                              textCapitalization: TextCapitalization.sentences,
                            ),
                            TextFormField(
                              key: RemindersKeys.itemField,
                              // maxLines: 10,
                              style: textTheme.subhead,
                              decoration: InputDecoration(
                                hintText: localizations.itemHint,
                              ),
                              validator: (val) {
                                return val.trim().isEmpty
                                    ? localizations.emptyItemError
                                    : null;
                              },
                              controller: itemController,
                              textInputAction: TextInputAction.next,
                              focusNode: _focusNodeItem,
                              onSaved: (value) =>
                                  _reminder = _reminder.copy(item: value),
                              onFieldSubmitted: (value) {
                                _focusNodeItem.unfocus();
                                // TODO: open datePicker
                              },
                              maxLength: 48,
                              maxLines: 1,
                              textCapitalization: TextCapitalization.sentences,
                            ),
                            DatePicker(
                              date: _reminder.expires,
                              selectDate: (date) {
                                setState(() {
                                  _reminder =
                                      _reminder.copy(expires: date.toString());
                                });
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: ReminderActionButton(
                            action: ReminderActionButtonActions.delete,
                            onTap: () {
                              print('delete');
                              _deleteReminder();
                            },
                            formKey: _formKey,
                          ),
                        ),
                        Container(
                          child: ReminderActionButton(
                            action: ReminderActionButtonActions.update,
                            onTap: () {
                              print('save');
                              _addEditReminder();
                            },
                            formKey: _formKey,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Container _getToolbar(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: BackButton(color: Colors.white),
    );
  }

  _updateVehicle() {
    setState(() {
      _reminder = _reminder.copy(vehicle: vehicleController.text.trim());
    });
  }

  _updateItem() {
    setState(() {
      _reminder = _reminder.copy(item: itemController.text.trim());
    });
  }

  _addEditReminder() {
    final form = _formKey.currentState;

    if (form.validate()) {
      HapticFeedback.heavyImpact();
      form.save();
      ReminderListModel model = ReminderListModel.of(context);
      if (isEditing) {
        model.updateReminder(_reminder);
      } else {
        model.addReminder(_reminder);
      }
      Future.delayed(const Duration(milliseconds: 150 + 50), () {
        Navigator.pop(
            context,
            ReminderUpdateAction(
              _reminder,
              ReminderUpdateActionType.update,
            ));
        HapticFeedback.lightImpact();
      });
    }
  }

  _deleteReminder() {
    HapticFeedback.lightImpact();
    if (widget.reminderId != null) {
      // has a reminder
      ReminderListModel model = ReminderListModel.of(context);
      Reminder reminder = model.reminderById(widget.reminderId);
      model.removeReminder(reminder);

      Future.delayed(const Duration(milliseconds: 150 + 50), () {
        Navigator.pop(
            context,
            ReminderUpdateAction(
              _reminder,
              ReminderUpdateActionType.delete,
            ));
        HapticFeedback.heavyImpact();
      });
      // Navigator.pop(context, reminder);
    } else {
      // no reminder
      Future.delayed(const Duration(milliseconds: 150 + 50), () {
        Navigator.pop(
            context,
            ReminderUpdateAction(
              null,
              ReminderUpdateActionType.cancel,
            ));
        HapticFeedback.heavyImpact();
      });
    }
  }
}
