// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'dart:io';

import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_car/localization.dart';
import 'package:my_car/notifier.dart';
import 'package:my_car/prefs.dart';
import 'package:my_car/reminder.dart';
import 'package:my_car/reminder_item.dart';
import 'package:my_car/reminder_model.dart';
import 'package:my_car/reminders_keys.dart';
import 'package:my_car/screens/add_edit_reminder_screen.dart';
import 'package:my_car/sharer.dart';
import 'package:my_car/widgets/feature_discovery.dart';
import 'package:scoped_model/scoped_model.dart';

// String _bannerId = Platform.isAndroid
//     ? 'ca-app-pub-9563432334416749/5063677152'
//     : 'ca-app-pub-9563432334416749/7965172668';
// String _interstitialId =
// String _rewardVideoId = Platform.isAndroid
//     ? 'ca-app-pub-9563432334416749/5746203437'
//     : 'ca-app-pub-9563432334416749/1754863823';

// BannerAd _bannerAd = BannerAd(
//   adUnitId: _bannerId,
//   size: AdSize.smartBanner,
//   targetingInfo: MobileAdTargetingInfo(
//     childDirected: false,
//     keywords: ['car', 'maintenance', 'car parts'],
//   ),
// );


class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HomeScreenState();
}

class HomeScreenState<HomeScreen> extends State with WidgetsBindingObserver {
  InterstitialAd _interstitialAd;
  bool _notificationsEnabled = true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    FirebaseAdMob.instance.initialize(
        appId: Platform.isAndroid
            ? 'ca-app-pub-9563432334416749~7526188963'
            : 'ca-app-pub-9563432334416749~5913724398');
    _loadInterstitial();
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print('didChangeDependencies');
    () async {
      if (Prefs.isLoading) {
        await Prefs.getPrefs();
        setState(() {});
        initFirstTime();
      }
    }();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
      initFirstTime();
    }
  }

  void initFirstTime() {
    Notifier.init(context).then((notificationsEnabled) {
      setState(() {
        _notificationsEnabled = notificationsEnabled;
        if (_notificationsEnabled) {
          showAddReminderFeature();
        }
      });
    });
  }

  void showAddReminderFeature() {
    if (!Prefs.isLoading) {
      if (!Prefs.getShownAddReminderFeature()) {
        Future.delayed(Duration(milliseconds: 750), () {
          FeatureDiscovery.discoverFeatures(context, ['FEATURE_ADD_REMINDER']);
          Prefs.setShownAddReminderFeature(true);
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<ReminderListModel>(
      builder: (context, child, model) {
        return Scaffold(
          appBar: AppBar(
            title: Text(RemindersLocalizations.of(context).appTitle),
            actions: [
              DescribedFeatureOverlay(
                featureId: 'FEATURE_SHARE',
                icon: Icons.share,
                color: Colors.red,
                title: RemindersLocalizations.of(context).featureShareTitle,
                doAction: shareApp,
                description:
                    RemindersLocalizations.of(context).featureShareText,
                child: IconButton(
                  icon: Icon(Icons.share),
                  onPressed: shareApp,
                ),
              ),
            ],
          ),
          body: Builder(builder: (BuildContext context) {
            // final bool somethingIsLoading = model.isLoading | Prefs.isLoading;
            if (!_notificationsEnabled) {
              return _buildNotificationsDisabled(context);
            }
            if (model.isLoading || Prefs.isLoading) {
              return _buildLoading();
            }
            if (model.isEmpty) {
              return _buildEmpty(context);
            }
            return _buildList(context, model);
          }),
          floatingActionButton: Builder(
            builder: (BuildContext context) {
              return _notificationsEnabled &&
                      !(model.isLoading || Prefs.isLoading)
                  ? _buildFAB(context, model)
                  : Container();
            },
          ),
        );
      },
    );
  }

  Widget _buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget _buildEmpty(context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Stack(
            children: <Widget>[
              Transform.rotate(
                angle: 3.1415892 / 180 * -5,
                child: Icon(
                  Icons.directions_car,
                  size: MediaQuery.of(context).size.height * 0.33,
                  color: Theme.of(context).accentColor,
                ),
              ),
              Positioned(
                child: !true
                    ? Image.asset(
                        'assets/imgs/triangle_full.png',
                        width: 120,
                      )
                    : Icon(
                        Icons.warning,
                        size: MediaQuery.of(context).size.height * 0.15,
                        color: Color.fromARGB(255, 255, 193, 7),
                      ),
                bottom: 0,
                right: 0,
              ),
            ],
          ),
          Text(
            RemindersLocalizations.of(context).noRemindersTitle,
            style: TextStyle(fontSize: 22.0),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 18.0,
          ),
          Text(
            RemindersLocalizations.of(context).noRemindersText,
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 18.0),
          SizedBox(
            height: 100,
          )
        ],
      ),
    );
  }

  Widget _buildNotificationsDisabled(context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.notifications_off,
            size: MediaQuery.of(context).size.height * 0.33,
            color: Theme.of(context).accentColor,
          ),
          Text(
            RemindersLocalizations.of(context).notifsDisabledTitle,
            style: TextStyle(fontSize: 22.0),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 18.0,
          ),
          Text(
            RemindersLocalizations.of(context).notifsDisabledText,
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 18.0),
          SizedBox(
            height: 100,
          )
        ],
      ),
    );
  }

  Widget _buildList(BuildContext homeContext, ReminderListModel model) {
    final reminders = model.sortedReminders;

    return CustomScrollView(
      scrollDirection: Axis.vertical,
      shrinkWrap: false,
      slivers: <Widget>[
        SliverPadding(
          padding: EdgeInsets.symmetric(vertical: 10.0),
          sliver: SliverList(
            key: RemindersKeys.reminderList,
            delegate: SliverChildBuilderDelegate(
              (_, index) {
                final reminder = reminders[index];
                return GestureDetector(
                  child: ReminderPreview(
                    reminder,
                  ),
                  onTap: () {
                    HapticFeedback.lightImpact();
                    Navigator.of(homeContext)
                        .push(
                      PageRouteBuilder(
                        pageBuilder: (_, __, ___) => AddEditScreen(
                              reminderId: reminder.id,
                            ),
                        transitionsBuilder: (homeContext, animation,
                                secondaryAnimation, child) =>
                            FadeTransition(
                              opacity: animation,
                              child: child,
                            ),
                      ),
                    )
                        .then((reminderUpdateAction) {
                      remindersUpdated(
                          model, homeContext, reminderUpdateAction);
                    });
                  },
                );
              },
              childCount: reminders.length,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildFAB(context, model) {
    return DescribedFeatureOverlay(
      featureId: 'FEATURE_ADD_REMINDER',
      icon: Icons.add,
      color: Colors.red,
      title: RemindersLocalizations.of(context).featureAddReminderTitle,
      description: RemindersLocalizations.of(context).featureAddReminderText,
      doAction: () {
        openAddReminder(model);
      },
      child: FloatingActionButton(
        key: RemindersKeys.addReminderFab,
        onPressed: () {
          openAddReminder(model);
        },
        child: Icon(Icons.add),
        tooltip: RemindersLocalizations.of(context).addReminder,
      ),
    );
  }

  void shareApp() {
    HapticFeedback.lightImpact();
    Sharer.share(context);
    Prefs.incrementNrTimesShared();
  }

  void openAddReminder(model) {
    HapticFeedback.lightImpact();
    Navigator.of(context)
        .push(PageRouteBuilder(
            pageBuilder: (_, __, ___) => AddEditScreen(),
            transitionsBuilder:
                (context, animation, secondaryAnimation, child) =>
                    FadeTransition(opacity: animation, child: child)))
        .then((reminderUpdateAction) {
      remindersUpdated(model, context, reminderUpdateAction);
    });
  }

  void remindersUpdated(
    ReminderListModel model,
    BuildContext context,
    ReminderUpdateAction reminderUpdateAction,
  ) async {
    await Future.delayed(Duration(milliseconds: 500));
    if (reminderUpdateAction != null) {
      Notifier.scheduleAll(model.sortedReminders);
      if (reminderUpdateAction.type == ReminderUpdateActionType.cancel) {
        // ???
        print('ReminderUpdateActionType.cancel');
      }
      if (reminderUpdateAction.type == ReminderUpdateActionType.delete) {
        if (reminderUpdateAction.reminder is Reminder) {
          _showUndoSnackbar(context, reminderUpdateAction.reminder);
        }
        print('ReminderUpdateActionType.delete');
      }
      if (reminderUpdateAction.type == ReminderUpdateActionType.update) {
        // ????
        Prefs.incrementNrRemindersAdded();
        print('ReminderUpdateActionType.update');
      }
      if (!Prefs.getShownShareAppFeature()) {
          FeatureDiscovery.discoverFeatures(context, ['FEATURE_SHARE']);
          Prefs.setShownShareAppFeature(true);
      } else {
          _showInterstitial();
      }
    }
  }

  void _showUndoSnackbar(BuildContext context, Reminder reminder) {
    Scaffold.of(context).showSnackBar(
      SnackBar(
        key: RemindersKeys.snackbar,
        duration: Duration(seconds: 5),
        backgroundColor: Theme.of(context).backgroundColor,
        content: Text(
          RemindersLocalizations.of(context).reminderDeleted(reminder.vehicle),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(color: Theme.of(context).textTheme.body1.color),
        ),
        action: SnackBarAction(
          key: RemindersKeys.snackbarAction(reminder.id),
          label: RemindersLocalizations.of(context).undo,
          onPressed: () {
            HapticFeedback.lightImpact();
            ReminderListModel.of(context).addReminder(reminder);
          },
        ),
      ),
    );
  }

  void _loadInterstitial() {
    _interstitialAd = InterstitialAd(
      adUnitId: Platform.isAndroid
          ? 'ca-app-pub-9563432334416749/8811350473'
          : 'ca-app-pub-9563432334416749/6652090996',
      targetingInfo: MobileAdTargetingInfo(
        childDirected: false,
        keywords: ['car', 'maintenance', 'car parts'],
      ),
    )..load();
  }

  void _showInterstitial() async {
    print('_showInterstitial');
    // _interstitialAd..show();
    // _loadInterstitial();
  }
}
